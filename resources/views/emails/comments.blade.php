@component('mail::message')
# New comment on the post

Someone posted a comment.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
