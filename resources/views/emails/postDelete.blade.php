@component('mail::message')
    # Post Deleted

    Your friend has deleted the post.


    @component('mail::button', ['url' => ''])
        Button Text
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
