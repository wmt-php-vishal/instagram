@component('mail::message')
# New Post Added

The body of your message.


New post added by {{$name}}
<br>
Caption of post - {{$text}}

{{--<img src="storage/{{$url}}" width="300px" height="200px"></img>--}}
<img src="{{$url}}" width="300px" height="200px" >
{{--<img src="{{ $message->embed($url) }}" width="300px" height="200px" >--}}



@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
