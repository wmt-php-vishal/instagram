@extends('layouts.app')
@section('title' , 'Display Post')

@section('js')

<!-- Theme JS files -->
<!-- Theme JS files -->

<!-- <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_bootstrap_select.js')}}"></script>

<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js')}}"></script> -->

<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>

<script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_selectbox.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/selectboxit.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/plugins/ui/ripple.min.js')}}"></script>
<!-- /theme JS files -->

@endsection

@section('content')

<div class="container text-center">
    <div class="panel panel-flat">
        <div class="panel-body">
            <h3 class="text-center">News Feed</h3>


            {{count($post) . " Posts found"}}
            <br><br>


            <form action="{{ route('post.index') }}" class="form-horizontal" method="get">

                <select name="countrySelect" class="selectbox form-control ">
                    <option value="">Select Country</option>
                    @foreach($country as $singleCountry)
                    <option value="{{$singleCountry->id}}">{{$singleCountry->name}}</option>
                    @endforeach
                </select>
                <button type="submit" value="show" class="my-5 btn btn-primary">Show</button>
            </form>

        </div>
    </div>

    @foreach($post as $singlePost)
    <div class="panel panel-flat">
        <div class="panel-body">
            {{ $loop->iteration}} <b> {{$singlePost->user->name}}</b> posted a {{$singlePost->post_type}}
            @if(auth::id() == $singlePost->user_id)
            <form action="{{ route('post.destroy' , ['post' => $singlePost->id])}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit">Delete Post</button>
            </form>
            @endif
            <br>

            @if($singlePost->post_type == 'photo')
            <img src="storage/{{$singlePost->url}}" width="300px" height="200px"></img>
            @endif
            @if($singlePost->post_type == 'video')
            <video src="storage/{{ $singlePost->url}}" width="300px" height="200px" controls></video>
            @endif
            <br>
            <b> {{$singlePost->user->name}}</b>
            <div class="text-wrap"> {{'- ' .$singlePost->text}}</div>


            {{--        @dd($singlePost->check_like())--}}
            @if(sizeof($singlePost->check_like) == null)
            <a href="{{ route('post.like' , ['id' => $singlePost->id] ) }}">like</a>
            @else
            <a href="{{ route('post.dislike' , ['id' => $singlePost->id] ) }}">dislike</a>
            @endif
            <br>
            <b>Comments</b><br>
            @foreach($singlePost->comment as $comment)

            <b>{{$comment->user->name}}</b> {{$comment->text}}
            <br>
            @endforeach


            <form action="{{ route('post.comment' , ['id'=> $singlePost->id])}}" method="post">
                @csrf
                <b class="col-md-2 offset-3">{{auth::user()->name}} </b>
                <input type="text" class="col-md-3 form-control"
                    name="comment" placeholder="Comment here">
{{--                <div style="color: red">--}}
{{--                    @error('comment')--}}
{{--                    {{$message}}--}}
{{--                    @enderror--}}
{{--                </div>--}}


                <br>
                <button type="submit" class="btn  btn-primary btn-rounded">Comment</button>
                <br>
            </form>
            <br><br><br>
        </div>
    </div>
    @endforeach

    @endsection

    <script>
    (".selectbox").selectBoxIt({
        autoWidth: false
    });
    </script>
</div>
