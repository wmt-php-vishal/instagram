<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Jobs\SendEmail;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('sendEmail' , function (){
////    Job::dispatch()->onQueue('emails');
//    SendEmail::dispatch()
//        ->delay(now()->addSeconds(5));
//
//    return "email sent successfully";
//});

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/post/{id}/like' , 'PostController@like')->name('post.like');
Route::any('/post/{id}/dislike' , 'PostController@dislike')->name('post.dislike');
Route::any('/post/{id}/comment' , 'PostController@commentStore')->name('post.comment');
Route::resource('post' , 'PostController');
//Route::resource('post' , 'PostController');

