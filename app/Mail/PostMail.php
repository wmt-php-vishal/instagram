<?php

namespace App\Mail;

use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class PostMail extends Mailable
{
    use Queueable, SerializesModels;

    private $post;

    /**
     * Create a new message instance.
     *
     * @return void

     */
    public function __construct($post)
    {
       $this->post = $post;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $file = $this->post->url;

        return $this->markdown('emails.post')
           ->with([
            'name' => $this->post->user->name,
            'url' => $this->post->url,
            'text' => $this->post->text,
            'message' => 'new photo'
       ])->attach($file);
    }
}
