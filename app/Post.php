<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class , 'post_id' , 'id');
    }



    public function getTextAttribute($value){
        return ucfirst($value);

    }
    public function likes()
    {
        return $this->belongsToMany(User::class,'post_like','','','','');
    }
    public function check_like()
    {
        return $this->likes()->where('user_id', Auth::id());
    }


    protected $guarded = [];
}
