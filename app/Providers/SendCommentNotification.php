<?php

namespace App\Providers;

use App\Comment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCommentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Comment  $event
     * @return void
     */
    public function handle(Comment $event)
    {
        //
    }
}
