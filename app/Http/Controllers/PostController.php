<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Country;
use App\Events\Comments;
use App\Http\Requests\PostRequest;
use App\Jobs\commentMailJob;
use App\Jobs\SendEmail;
use App\Mail\PostMail;
use App\Post;
use App\post_like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Constraint\Count;
use App\Mail\RegistrationMail;
use Illuminate\Support\Facades\Mail;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
            //
        }
        else{
            return view('auth/login');
        }
        if (isset(\request()->countrySelect)) {

            $post = Country::find(\request()->countrySelect)->post;
        } else {
            $post = Post::with('comment','check_like')->get();

        }
//        dd($post[0]->check_like);
        $country = Country::all();
//        $post = Post::find(83);
//        dd($post->likes());

        return view('post/displayPost')->with(['post' => $post])->with('country', $country);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user())
        {
            return view('post/addPost');
        }
        else{
            return view('auth/login');
        }

    }

    /**
     * Store a newly created resource in comment storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function commentStore(Request $request, $id)
    {
        $validatedData = $request->validate([
            'comment' => 'required|max:255',

        ]);

        $comments = Comment::with('Post')->get();
//        dd($comments[0]->text);
        $comment = Comment::create([
            'post_id' => $id,
            'user_id' => auth::id(),
            'text' => $request->comment,
        ]);

        commentMailJob::dispatch()
            ->delay(now()->addSeconds(5));

        event(new Comments('commentListener has sent an email on your address'));
        return back()->with(['comments' => $comments]);

    }


    /**
     * Store a newly created resource in comment storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

public function like($id)
{
    $post = Post::find($id);
    $post->likes()->attach(['user_id' => auth::id()]);
    return back();
}

public function dislike($id)
{
    $post = Post::find($id);
    $post->likes()->detach(['user_id' => auth::id()]);
    return back();
}

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
//        $validatedData = $request->validate([
//            'file' => 'mimes:jpeg,png,jpg,avi,mp4,mov,svg',
//            'caption' => 'required'
//        ]);

        $url = $request->file('file');

        $extension = $url->getClientOriginalExtension();

        $urlPath = $url->storeAs('post', $url->getClientOriginalName(), 'public');


        if ($extension == "jpeg" || $extension == "jpg" || $extension == "svg" || $extension == "png") {
            $postType = 'photo';

        }

        if ($extension == "mp4" || $extension == "mov" || $extension == "avi") {
            $postType = 'video';
        }

        $post = Post::create([
            'user_id' => Auth::id(),
            'url' => $urlPath,
            'text' => $request->caption,
            'post_type' => $postType
        ]);
//        dd($urlPath);

        Mail::to('test@gmail.com')->send(new PostMail($post));
            // return redirect()->route('post.index');
        return redirect()->action('PostController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return back();

    }
}
